import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Profile } from '../models';

@Injectable()
export class ProfilesService {
  constructor (
    private apiService: ApiService
  ) {}

  get(username: string): Observable<Profile> {
    return this.apiService.get('/users/profiles/' + username)
           .map((data: {profile: Profile}) => data.profile);
  }

  follow(username: string): Observable<Profile> {
    return this.apiService.post('/users/profiles/' + username + '/follow', {user_id:localStorage.getItem('user_id')});
  }

  unfollow(username: string): Observable<Profile> {
    return this.apiService.post('/users/profiles/' + username + '/unfollow', {user_id:localStorage.getItem('user_id')});
  }

}
