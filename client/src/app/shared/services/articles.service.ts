import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { ApiService } from './api.service';
import { Article, ArticleListConfig } from '../models';

@Injectable()
export class ArticlesService {
  constructor (
    private apiService: ApiService
  ) {}
  private user_id = localStorage.getItem("user_id");
  query(config: ArticleListConfig): Observable<{articles: Article[], articlesCount: number}> {
    console.log(config);
    // Convert any filters over to Angular's URLSearchParams
    let params: URLSearchParams = new URLSearchParams();

    Object.keys(config.filters)
    .forEach((key) => {
      params.set(key, config.filters[key]);
    });
    return this.apiService
    .get(
      '/articles' + ((config.type === 'feed') ? '/feed' : ''),
      params
    ).map(data => data);
  }

  get(slug): Observable<Article> {
    return this.apiService.get('/articles/slug/' + slug)
           .map(data => data.article);
  }

  destroy(slug) {
    return this.apiService.delete('/articles/' + slug);
  }

  save(article): Observable<Article> {
    // mise a jours de l'article
    if (article.slug) {
      return this.apiService.put('/articles/' + article.slug, {article: article, user_id: localStorage.getItem("user_id")})
             .map(data => data.article);
    // création de l'article
    } else {
      return this.apiService.post('/articles/add', {article: article, user_id: localStorage.getItem("user_id")})
             .map(data => data.article);
    }
  }

  favorite(slug): Observable<Article> {
    return this.apiService.post('/articles/' + slug + '/favorite', {user_id:localStorage.getItem('user_id')})
      .map(data => data);
  }

  unfavorite(slug): Observable<Article> {
    return this.apiService.post('/articles/' + slug + '/unfavorite', {user_id:localStorage.getItem('user_id')});
  }


}
