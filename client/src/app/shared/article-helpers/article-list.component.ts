import {Component, Input} from '@angular/core';

import {Article, ArticleListConfig} from '../models';
import {ArticlesService} from '../services';

@Component({
  selector: 'article-list',
  templateUrl: './article-list.component.html'
})
export class ArticleListComponent {
  constructor(private articlesService: ArticlesService) {
  }

  @Input() limit: number;

  @Input()
  set config(config: ArticleListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: ArticleListConfig;
  results: Article[];
  loading: boolean = false;
  currentPage: number = 1;
  totalPages: Array<number> = [1];

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    this.loading = true;
    this.results = [];
    // Création des paramêtres de requête
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.offset = (this.limit * (this.currentPage - 1));
    }
    if(this.query.type == 'feed'){
      this.query.filters.author = localStorage.getItem("user_id");
    }
    this.articlesService.query(this.query)
      .subscribe(data => {
        this.loading = false;
        this.results = data.articles;
        this.totalPages = Array.from(new Array(Math.ceil(5 / this.limit)), (val, index)=>index + 1);
      });
  }
}
