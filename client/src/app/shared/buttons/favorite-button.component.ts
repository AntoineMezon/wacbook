import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from '@angular/router';

import {Article} from '../models';
import {ArticlesService, UserService} from '../services';

@Component({
  selector: 'favorite-button',
  templateUrl: './favorite-button.component.html'
})
export class FavoriteButtonComponent {
  constructor(private articlesService: ArticlesService,
              private router: Router,
              private userService: UserService) {
  }

  @Input() article: Article;
  @Output() onToggle = new EventEmitter<boolean>();
  isSubmitting = false;


  toggleFavorite() {
    this.isSubmitting = true;

    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        // auth guard
        if (!authenticated) {
          this.router.navigateByUrl('/login');
          return;
        }

        //envois la demande au service au click
        this.articlesService.favorite(this.article.slug)
          .subscribe(
            data => {
              console.log(data);
              if (data['favorite']) {
                this.isSubmitting = false;
                this.onToggle.emit(true);
              } else {
                this.isSubmitting = false;
                this.onToggle.emit(false);
              }
            },
            err => this.isSubmitting = false
          );
      }
    )


  }

}
