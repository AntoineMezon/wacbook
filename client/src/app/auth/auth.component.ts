import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {Errors, UserService} from '../shared';

@Component({
  selector: 'auth-page',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {
  authType: String = '';
  title: String = '';
  errors: Errors = new Errors();
  isSubmitting: boolean = false;
  authForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fb: FormBuilder) {
    this.authForm = this.fb.group({
      'email': ['', Validators.required],
      'password': ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.url.subscribe(data => {
      // récupère l'url : login|register
      this.authType = data[data.length - 1].path;
      this.title = (this.authType === 'login') ? 'Se connecter' : 'S\'inscrire';
      // ajout du champ username si la page est register
      if (this.authType === 'register') {
        this.authForm.addControl('username', new FormControl());
      }
    });
  }

  submitForm() {
    localStorage.clear();
    this.isSubmitting = true;
    this.errors = new Errors();

    let credentials = this.authForm.value;
    this.userService
      .attemptAuth(this.authType, credentials)
      .subscribe(
        data => {
          localStorage.setItem("user_id", data['user'].id);
          localStorage.setItem("username", data['user'].username);
          console.log(localStorage.getItem("user_id"));
          this.router.navigateByUrl('/');
        },
        err => {
          this.errors = err;
          this.isSubmitting = false;
        })
  }

  Zzz
}
