var db = require('../db.connect');

module.exports = {
    //récupère tout les tags
    list: function () {
        return db.tags.findAll({
        })
    },
    //récupère les tags d'un article
    ArticleTag: function(id){
        return db.tags.findAll({
            where:{
                tag_post_id: id
            },
            attributes:['content']
        })
    },
    //ajout d'un tag
    add: function (id, body) {
        return db.tags.create({
            tag_post_id: id,
            content: body
        })
    },
    //supprime un tag
    delete:function (id) {
        return db.tags.destroy({
            where:{
                tag_post_id:id
            }
        })
    }
};