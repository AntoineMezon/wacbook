var db = require('../db.connect');

module.exports = {
    //ajout d'un commentaire
    add: function (id, body) {
        return db.comments.create({
            comment_post_id: id,
            comment_user_id: body.comment.user_id,
            body: body.comment.body,
            include: [{
                model: db.users,
                attributes: ["username", "image"],
            }],
        })
    },
    //récupérer tout les commentaire d'un article
    getAll: function (id) {
        return db.comments.findAll({
            where: {
                comment_post_id: id
            },
            include: [{
                model: db.users,
                attributes: ["username", "image"],
            }]
        })
    },
    //récupérer un commentaire
    getOne: function (id) {
        return db.comments.findOne({
            where: {
                id: id
            },
            include: [{
                model: db.users,
                attributes: ["username", "image"],
            }],
        })
    },
    //Supression d'un commentaire
    delete: function (id) {
        return db.comments.destroy({
            where: {id: id}
        })
    }
}
