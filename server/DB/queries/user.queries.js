var db = require('../db.connect');

module.exports = {
    //ajout d'un utilisateur
    register: function (body, token) {
        return db.users.create({
            username: body.username,
            email: body.email,
            password: body.password,
            token: token,
            following: false
        })
    },
    //récupère un utilisateur
    login: function (email, password) {
        return db.users.findOne({
            where: {
                email: email,
                password: password
            }
        })
    },
    //récupère l'utilisateur authentifié
    getCurrent: function (id) {
        return db.users.findOne({
            where: {
                id: id
            }
        })
    },
    //récupère le profile d'un utilisateur
    getProfile: function (username) {
        return db.users.findOne({
            where: {
                username: username,
            }
        })
    },
    //vérifie l'existance d'un utilisateur
    issetUser: function (email) {
        return db.users.find(
            {
                where: {
                    email: email
                }
            }
        )
    },
    //recherche d'utilisateur
    searchUser: function (query) {
        return db.users.findAll({
                where: {
                    username: query,
                    $or: [
                        {'username': {like: '%' + query + '%'}},
                    ]
                }
            }
        )
    },
    //met a jour un utilisateur
    updateUser: function (user) {
        return db.users.update({
                username: user.username,
                email: user.email,
                image: user.image,
                bio: user.bio,
                password: user.password
            },
            {
                where: {
                    id: user.id
                }
            });
    },
    //met a jour l'utilisateur
    addFollow: function (username) {
        return db.users.update({
            following: true
        }, {
            where: {
                username: username
            }
        })
    },
    //met a jour l'utilisateur
    deleteFollow: function (username) {
        return db.users.update({
            following: false
        }, {
            where: {
                username: username
            }
        })
    }
};