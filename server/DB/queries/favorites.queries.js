var db = require('../db.connect');
//requetes sql de la table favorites
module.exports = {
    //récupère un favoris
    getOne: function (post_id, user_id) {
        return db.favorites.findOne({
            where: {
                favorite_post_id: post_id,
                favorite_user_id: user_id
            }
        })
    },
    //récupère les favoris d'un utilisateur
    getUser: function (user_id) {
        return db.favorites.findAll({
            where: {
                favorite_user_id: user_id
            },
        })
    },
    //créer un favoris
    favorite: function (post_id, user_id) {
        return db.favorites.create({
            favorite_user_id: user_id,
            favorite_post_id: post_id,
            follow: true
        })
    },
    //supprime un favoris
    unfavorite: function (post_id, user_id) {
        return db.favorites.destroy({
            where: {
                favorite_user_id: user_id,
                favorite_post_id: post_id
            }
        })
    },
};
