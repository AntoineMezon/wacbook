var db = require('../db.connect');
//requête sql de la table posts
module.exports = {
    //ajouté un article
    add: function (body, id, slug) {
        console.log("obj", id);
        return db.posts.create({
            post_user_id: id,
            description: body.description,
            slug: slug,
            body: body.body,
            title: body.title
        })
    },
//récupérer tous les articles
    getAll: function (limit) {
        return db.posts.findAll({
            include: [{
                model: db.users,
                attributes: ["username", "image", "bio", "following"],
            }],
        })
    },
    //récupérer les articles d'un utilisateur
    getUserArticles: function (id) {
        return db.posts.findAll({
            where: {
                post_user_id: id
            },
            include: [{
                model: db.users,
                attributes: ["username", "bio", "image", "following"],
            }],
        })
    },
    //récupérer un article par son id
    getById: function (id) {
        return db.posts.findOne({
            where: {
                id: id
            },
            include: [{
                model: db.users,
                attributes: ["username", "bio", "image", "following"],
            }],
        })
    },
    //récupérer un article par son slug
    getBySlug: function (slug) {
        return db.posts.findOne({
            where: {
                slug: slug
            },
            include: [{
                model: db.users,
                attributes: ["username", "bio", "image", "following"],
            }],
        })
    },
    //récupère un article par son slug
    getOne: function (slug) {
        return db.posts.findOne({
            where: {
                slug: slug
            },
            include: [{
                model: db.users,
                attributes: ["username", "bio", "image", "following"],
            }],
        })
    },
    //récupère les articles par tags
    getPublicationWithTag: function (tag) {
        return db.tags.findAll({
            include: [{
                model: db.posts,
                attributes: ['id']
            }],
            where: {
                content: tag
            }
        })
    },
    //met a jours un article
    update: function (article, newSlug) {
        return db.posts.update({
                slug: newSlug,
                title: article.title,
                description: article.description,
                body: article.body
            },
            {
                where: {
                    id: article.id,
                    post_user_id: article.post_user_id
                }
            });
    },
    //mise a jour du nombre de favoris d'un article
    addFavorite: function (slug, countLike) {
        return db.posts.update({
                favorited: true,
                favoritesCount: countLike +1,
            },
            {
                where: {
                    slug: slug
                }
            });
    },
    //mise a jour du nombre de favoris d'un article
    deleteFavorite: function (slug, countLike) {
        return db.posts.update({
                favorited: true,
                favoritesCount: countLike -1,
            },
            {
                where: {
                    slug: slug
                }
            });
    },
    //supprime un article
    delete: function (slug) {
        return db.posts.destroy({
            where: {slug: slug}
        })
    }
}
