var db = require('../db.connect');

module.exports = {
    //récupère un follow
    getOne: function (follower_id, user_id) {
        return db.follows.findOne({
            where: {
                followed_user_id: user_id,
                follower_user_id: follower_id
            }
        })
    },
    //crée un follow
    follow: function (follower_id, user_id) {
        return db.follows.create({
            followed_user_id: user_id,
            follower_user_id: follower_id
        })
    },
    //supprime un follow
    unFollow: function (follower_id, user_id) {
        return db.follows.destroy({
            where: {
                followed_user_id: user_id,
                follower_user_id: follower_id
            }
        })
    },
    //récupère l'utilsateur followed
    getUser: function (user_id) {
        return db.follows.findAll({
            where: {
                followed_user_id: user_id
            },
        })
    },
};
