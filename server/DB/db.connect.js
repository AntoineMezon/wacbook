var Sequelize = require('sequelize');

//connection a la base de données
const sequelize = new Sequelize('wacbook', 'root', 'toor', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false,
    define: {
        underscored: true
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Models/tables
db.users = require('./models/users')(sequelize, Sequelize);
db.posts = require('./models/posts')(sequelize, Sequelize);
db.comments = require('./models/comments')(sequelize, Sequelize);
db.tags = require('./models/tags')(sequelize, Sequelize);
db.favorites = require('./models/favorites')(sequelize, Sequelize);
db.follows = require('./models/follow')(sequelize, Sequelize);
//Relations
db.sequelize
    .query('SET FOREIGN_KEY_CHECKS = 0', {raw: true})
    .then(function (results) {
        db.sequelize.sync({force: true});
    });
//Relations des tables
db.posts.belongsTo(db.users, {foreignKey: 'post_user_id', foreignKeyConstraint: true});
db.users.hasMany(db.posts, {foreignKey: 'post_user_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.tags.belongsTo(db.posts, {foreignKey: 'tag_post_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.posts.hasMany(db.tags, {foreignKey: 'tag_post_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.comments.belongsTo(db.posts, {foreignKey: 'comment_post_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.posts.hasMany(db.comments, {foreignKey: 'comment_post_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.comments.belongsTo(db.users, {foreignKey: 'comment_user_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.users.hasMany(db.comments, {foreignKey: 'comment_user_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.favorites.belongsTo(db.users, {foreignKey: 'favorite_user_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.users.hasMany(db.favorites, {foreignKey: 'favorite_user_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.favorites.belongsTo(db.posts, {foreignKey: 'favorite_post_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.posts.hasMany(db.favorites, {foreignKey: 'favorite_post_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.follows.belongsTo(db.users, {foreignKey: 'followed_user_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.users.hasMany(db.follows, {foreignKey: 'followed_user_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

db.follows.belongsTo(db.users, {foreignKey: 'follower_user_id', foreignKeyConstraint: true, onDelete:'CASCADE'});
db.users.hasMany(db.follows, {foreignKey: 'follower_user_id', foreignKeyConstraint: true, onDelete: 'CASCADE'});

module.exports = db;


