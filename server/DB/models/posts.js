'use strict';
//définition du model posts
module.exports = function (Sequelize, DataTypes) {
    return Sequelize.define('posts', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        post_user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        comment_count: {
            type: DataTypes.INTEGER(11)
        },
        slug: {
            type: DataTypes.STRING
        },
        title: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.STRING
        },
        body: {
            type: DataTypes.STRING
        },
        favorited: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        favoritesCount: {
            type: DataTypes.INTEGER(11),
            defaultValue: 0
        },
        is_public: {
            type: DataTypes.BOOLEAN
        },
        is_private: {
            type: DataTypes.BOOLEAN
        },
        created_at: {
            type: DataTypes.DATE
        },
        updated_at: {
            type: DataTypes.DATE
        }
    }, {
        underscored: true
    });
};