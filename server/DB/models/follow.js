'use strict';
//définition du model follows
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('follows', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        follower_user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        followed_user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE
        },
        updated_at: {
            type: DataTypes.DATE
        }
    }, {
        underscored: true
    });
};