'use strict';
//définition du model favorites

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('favorites', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        favorite_user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        favorite_post_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        follow:{
            type:DataTypes.BOOLEAN,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE
        },
        updated_at: {
            type: DataTypes.DATE
        }
    }, {
        underscored: true
    });
};