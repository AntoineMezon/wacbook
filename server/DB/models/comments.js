'use strict';

//définition du model comments
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('comments', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
        },
        comment_post_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        comment_user_id: {
            type: DataTypes.UUID,
            allowNull: false
        },
        body: {
            type: DataTypes.TEXT,
            required: true
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updated_at:  DataTypes.DATE,
        deleted_at: DataTypes.DATE
    }, {
        underscored: true
    });
};