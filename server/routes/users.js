let express = require('express');
let router = express.Router();
let user_queries = require("../DB/queries/user.queries");
let follow_queries = require("../DB/queries/follow.queries");
let request = require("request");


module.exports = router;
router.route('/')
    .post(function (req, res) {
        //enregistre l'utilisateur s'il n'existe pas
        user_queries.issetUser(req.body.email).then(function (email) {
            if (!email) {
                user_queries.register(req.body.user, "logged").then(function (user) {
                    res.status(200).send({
                        success: "Votre compte a bien été créer. Cependant veuillez consulter vos mails",
                        user: user
                    });
                });
            } else {
                res.status(200).send({
                    error: "Email déjà utilisé"
                });
            }
        });

    }).get(function (req, res) {
    //récupère l'utilisateur authentifié
    user_queries.getCurrent().then(function (result) {
        res.status(200).send({
            user: result
        });
    });
});

router.route('/login')
    .post(function (req, res) {
        //authentifie l'utilisateur
        user_queries.login(req.body.user.email, req.body.user.password).then(function (user) {
            if (user == null) {
                res.status(200).send({
                    error: "Unknow"
                });
            } else {
                res.status(200).send({
                    user: user
                });
            }
        });
    });
//met a jour l'utilisateur
router.put('/', function (req, res, next) {
    var user = req.body.user;
    user_queries.updateUser(user).then(function (success) {
        user_queries.getCurrent(user.id).then(function (user) {
            res.status(200).send({
                user: user
            });
        });
    });
});

//récupère le profile d'un utilisateur
router.get('/profiles/:name', function (req, res) {
    user_queries.getProfile(req.params.name).then(function (profile) {
        res.status(200).send({
            profile: profile
        });
    })
});
//crée un follow et met a jour l'utilateur
router.post('/profiles/:name/follow', function (req, res) {
    let username = req.params.name;
    let followers_id = req.body.user_id;
    user_queries.getProfile(username).then(function (user) {
        follow_queries.getOne(followers_id, user.id).then(function (follow) {
            if (!follow) {
                user_queries.addFollow(username).then(function () {
                follow_queries.follow(followers_id, user.id).then(function () {
                    user_queries.getProfile(username).then(function (profile) {
                        res.status(200).send({article: profile});
                    })
                })
                })
            }
        });
    });
});
//supprime un follow et met a jour l'utilateur
router.post('/profiles/:name/unfollow', function (req, res) {
    let username = req.params.name;
    let followers_id = req.body.user_id;
    user_queries.getProfile(username).then(function (user) {
        follow_queries.getOne(followers_id, user.id).then(function (follow) {
            if (follow) {
                user_queries.deleteFollow(username).then(function () {
                    follow_queries.unFollow(followers_id, user.id).then(function () {
                        user_queries.getProfile(username).then(function (profile) {
                            res.status(200).send({article: profile});
                        })
                    })
                })
            }
        });
    });
});

module.exports = router;

