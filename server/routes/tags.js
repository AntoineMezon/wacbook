var express = require('express');
var router = express.Router();
var request = require('request');
let tags_queries = require("../DB/queries/tags.queries.js");


router.get('/', function (req, res, next) {
    //récupère tous les tags
    tags_queries.list().then(function (tags) {
        if(!tags){
            res.status(200).send({err: "no tags"});
        } else {
            res.status(200).send({tags: tags});
        }
    })
});


module.exports = router;