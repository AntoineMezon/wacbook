var express = require('express');
var router = express.Router();
var request = require('request');
let posts_queries = require("../DB/queries/posts.queries.js");
let comments_queries = require("../DB/queries/commentaire.queries.js");
let tags_queries = require("../DB/queries/tags.queries.js");
let fav_queries = require("../DB/queries/favorites.queries.js");
let users_queries = require("../DB/queries/user.queries");
var queryString = require('querystring');
var bb = require('bluebird');
let Q = require('q');

//récupère les articles par tags, utilisateur connecté, favoris
router.get('/?', function (req, res, next) {
    let offset = req.param('offset');
    let limit = req.param('limit');
    let tag = req.param('tag');
    let feed = req.param('feed');
    let favorited = req.param('favorited');
    let author = req.param('author');
    let result = [];
    if (tag) {
        //récupère les articles par tag
        posts_queries.getPublicationWithTag(tag).then(function (tags) {
            tags.forEach(function (tag) {
                var deferred = Q.defer();
                posts_queries.getById(tag.post.id).then(function (post) {
                    var str = JSON.stringify(post);
                    var obj = JSON.parse(str);
                    obj['author'] = obj['user'];
                    delete obj['user'];
                    deferred.resolve(obj);
                });
                result.push(deferred.promise);
            });
            Q.all(result).then(function (result) {
                res.status(200).send({
                    articles: result
                });
            });
        });
    } else if (favorited) {
        //récupère les articles favoris de l'utilisateur connecté
        users_queries.getProfile(favorited).then(function (user) {
            fav_queries.getUser(user.id).then(function (favorites) {
                favorites.forEach(function (fav) {
                    var deferred = Q.defer();
                    posts_queries.getById(fav.favorite_post_id).then(function (post) {
                        var str = JSON.stringify(post);
                        var obj = JSON.parse(str);
                        obj['author'] = obj['user'];
                        delete obj['user'];
                        deferred.resolve(obj);
                    });
                    result.push(deferred.promise);
                });
                Q.all(result).then(function (result) {
                    res.status(200).send({
                        articles: result
                    });
                });
            });
        });
    } else if (author){
        //récupère les articles de l'utilisateur connecté
        users_queries.getProfile(author).then(function (user) {
            posts_queries.getUserArticles(user.id).then(function (articles) {
                let result = [];
                for (let i in articles) {
                    var str = JSON.stringify(articles[i]);
                    var object = JSON.parse(str);
                    object['author'] = articles[i]['user'];
                    delete object['user'];
                    result.push(object);
                }
                res.status(200).send({
                    articles: result
                });
            });
        });
    }else {
        //récupère tous les articles
        posts_queries.getAll(limit).then(function (articles) {
            for (let i in articles) {
                var str = JSON.stringify(articles[i]);
                var object = JSON.parse(str);
                object['author'] = articles[i]['user'];
                delete object['user'];
                result.push(object);
            }
            res.status(200).send({
                articles: result
            });
        });
    }
});
//récupère les articles de l'utilisateur connecté
router.get('/feed?', function (req, res, next) {
    posts_queries.getUserArticles(req.param('author')).then(function (articles) {
        let result = [];
        for (let i in articles) {
            var str = JSON.stringify(articles[i]);
            var object = JSON.parse(str);
            object['author'] = articles[i]['user'];
            delete object['user'];
            result.push(object);
        }
        res.status(200).send({
            articles: result
        });
    });
});

//Ajout d'un article
router.post('/add', function (req, res, next) {
    let tags = req.body.article['tagList'];
    let slug = req.body.article.title;
    let re = "-";
    let newSlug = slug.replace(' ', re);
    posts_queries.add(req.body.article, req.body.user_id, newSlug).then(function (publication) {
        if (publication) {
            for (let i in tags) {
                tags_queries.add(publication.id, tags[i]).then(function () {

                });
            }
            res.status(200).send({
                article: publication
            });
        } else {
            res.status(200).send({
                err: "err"
            });
        }
    });
});
//Renvois l'article par son slug
router.get('/slug/:name', function (req, res, next) {
    var name = req.params.name;
    posts_queries.getOne(name).then(function (article) {
        tags_queries.ArticleTag(article.id).then(function (tags) {
            var str = JSON.stringify(article);
            var object = JSON.parse(str);
            object['author'] = article['user'];
            object['tagList'] = [];
            delete object['user'];
            for (let i in tags) {
                object['tagList'].push(tags[i].dataValues.content);
            }
            res.status(200).send({
                article: object
            });
        });
    });
});

//supprime l'article par son slug
router.delete('/:slug', function (req, res, next) {
    var slug = req.params.slug;
    posts_queries.delete(slug).then(function (r) {
        res.status(200).send({
            err: "err"
        });
    });
});

//ajout d'un commentaire
router.post('/:slug/comments', function (req, response, next) {
    var slug = req.params.slug;
    posts_queries.getOne(slug).then(function (article) {
        comments_queries.add(article.id, req.body).then(function (newComment) {
            comments_queries.getOne(newComment.id).then(function (comment) {
                let str = JSON.stringify(comment);
                let obj = JSON.parse(str);
                obj['author'] = {username: obj.user.username};
                delete obj['user'];
                response.status(200).send({
                    comment: obj
                });
            });
        });
    });
});
//liste des commentaires par article
router.get('/:slug/comments', function (req, response, next) {
    var slug = req.params.slug;
    posts_queries.getBySlug(slug).then(function (article) {
        comments_queries.getAll(article.id).then(function (comments) {
            result = [];
            for (let i in comments) {
                let str = JSON.stringify(comments[i]);
                let obj = JSON.parse(str);
                obj['author'] = obj['user'];
                delete obj['user'];
                result.push(obj);
            }
            response.status(200).send({
                comments: result
            });
        });
    });
});
//supprime un commentaire
router.delete('/comments/:id', function (req, response, next) {
    var id = req.params.id;
    comments_queries.delete(id).then(function () {
        response.status(200).send({});
    });
});
//Met à jour l'article
router.put('/:slug', function (req, res, next) {
    var body = req.body.article;
    var newSlug = body.title.replace(' ', '-');
    posts_queries.update(body, newSlug).then(function (success) {
        posts_queries.getOne(newSlug).then(function (article) {
            var str = JSON.stringify(article);
            var object = JSON.parse(str);
            tags_queries.delete(object.id).then(function () {
                for (let i in body.tagList) {
                    tags_queries.add(object.id, body.tagList[i]).then(function (tag) {
                    });
                }
            }).then(function () {
                object['author'] = article['user'];
                delete object['user'];
                tags_queries.ArticleTag(object.id).then(function (tags) {
                    for (let i in tags) {
                        object['tagList'].push(tags[i].dataValues.content);
                    }
                    res.status(200).send({article: object});
                });
            });
        })
    });
});

//ajout ou supprime un like d'un article selon l'utilisateur
router.post('/:slug/favorite', function (req, res, next) {
    let slug = req.params.slug;
    let user_id = req.body.user_id;
    posts_queries.getOne(slug).then(function (article) {
        let countLike = article.favoritesCount;
        fav_queries.getOne(article.id, user_id).then(function (favorite) {
            if (!favorite) {
                posts_queries.addFavorite(req.params.slug, countLike).then(function () {
                    fav_queries.favorite(article.id, user_id).then(function () {
                        posts_queries.getOne(slug).then(function (article) {
                            res.status(200).send({article: article, favorite:true});
                        })
                    });
                });
            } else {
                posts_queries.deleteFavorite(req.params.slug, countLike).then(function () {
                    fav_queries.unfavorite(article.id, user_id).then(function () {
                        posts_queries.getOne(slug).then(function (articleUpdated) {
                            res.status(200).send({article: articleUpdated , unfavorite: true});
                        });
                    });
                });
            }
        });
    })
});
module.exports = router;