var express = require('express');
var router = express.Router();
var request = require('request');
let user_queries = require("../DB/queries/user.queries");
var queryString = require('querystring');


router.get('/search', function (req, res, next) {
    var query = require('url').parse(req.url, true).search;
    var sub = query.substring(1);
    var sub1 = sub.substring(1);
    var sub2 = sub1.substring(1);
    user_queries.searchUser(sub2).then(function (result) {
        if(result){
            res.status(200).send({data:result});
        } else {
            res.status(200).send({err:"no result"});
        }
    });
});

module.exports = router;